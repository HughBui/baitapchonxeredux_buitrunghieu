const imgCar = {
  RED_CAR: "./img/red-car.jpg",
  BLACK_CAR: "./img/black-car.jpg",
  SILVER_CAR: "./img/silver-car.jpg",
};

export default imgCar;
