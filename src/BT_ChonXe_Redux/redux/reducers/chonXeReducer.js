import { BLACK_CAR, RED_CAR, SILVER_CAR } from "../contants/contant";
import imgCar from "../contants/imgCar";

let initialState = {
  url: imgCar.BLACK_CAR,
};
export const chonXeReducer = (state = initialState, action) => {
  switch (action.type) {
    case RED_CAR: {
      state.url = imgCar.RED_CAR;
      return { ...state };
    }
    case BLACK_CAR: {
      state.url = imgCar.BLACK_CAR;
      return { ...state };
    }
    case SILVER_CAR: {
      state.url = imgCar.SILVER_CAR;
      return { ...state };
    }

    default:
      return state;
  }
};
