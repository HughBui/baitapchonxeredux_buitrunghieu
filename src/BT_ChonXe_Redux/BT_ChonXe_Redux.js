import React, { Component } from "react";
import { connect } from "react-redux";
import { BLACK_CAR, RED_CAR, SILVER_CAR } from "./redux/contants/contant";

class BT_ChonXe_Redux extends Component {
  render() {
    return (
      <div>
        {" "}
        <div className="container">
          <img
            src={this.props.imgCar}
            alt=""
            className={"w-50"}
            style={{ marginTop: "100px" }}
          />
          <div>
            <button
              onClick={this.props.handleChangeRedCar}
              className="btn btn-danger m-3"
            >
              RED
            </button>
            <button
              onClick={this.props.handleChangeBlackCar}
              className="btn btn-dark m-3"
            >
              BLACK
            </button>
            <button
              onClick={this.props.handleChangeSilverCar}
              className="btn btn-secondary m-3"
            >
              SILVER
            </button>
          </div>
        </div>
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    imgCar: state.chonXeReducer.url,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleChangeRedCar: () => {
      dispatch({ type: RED_CAR });
    },

    handleChangeBlackCar: () => {
      dispatch({ type: BLACK_CAR });
    },

    handleChangeSilverCar: () => {
      dispatch({ type: SILVER_CAR });
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(BT_ChonXe_Redux);
