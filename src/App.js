import logo from "./logo.svg";
import "./App.css";
import BT_ChonXe_Redux from "./BT_ChonXe_Redux/BT_ChonXe_Redux";

function App() {
  return (
    <div className="App">
      <BT_ChonXe_Redux />
    </div>
  );
}

export default App;
